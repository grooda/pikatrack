import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const admin_application_settings = new Vapi({
    baseURL: '/api',
    state: {
        admin_application_settings: []
    }
}).get({
    action: 'admin_application_settings_index',
    property: 'application_settings',
    path: '/admin/application_settings.json'
}).getStore({
    createStateFn: true
})

export default {
    state: admin_application_settings.state,
    mutations: admin_application_settings.mutations,
    actions: admin_application_settings.actions,
    getters: admin_application_settings.getters
}
