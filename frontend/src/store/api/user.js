import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const user = new Vapi({
    baseURL: '/api',
    state: {
        user: []
    }
}).get({
    action: 'show_user',
    property: 'user',
    path: ({userId}) => `user/${userId}.json`
}).getStore({
    createStateFn: true
})

export default {
    state: user.state,
    mutations: user.mutations,
    actions: user.actions,
    getters: user.getters
}
