import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const activities = new Vapi({
    baseURL: '/api',
    state: {
        activities: []
    }
}).get({
    action: 'show_activity',
    property: 'activities',
    path: ({id}) => `/activity/${id}.json`
}).delete({
    action: 'delete_activity',
    property: 'activities',
    path: ({id}) => `/activity/${id}`
}).post({
    action: 'recompute_activity',
    property: 'activities',
    path: ({id}) => `/activity/${id}/recompute`
}).get({
    action: 'get_activity_index',
    property: 'activities',
    path: ({userId}) => `user/${userId}/activities.json`
}).getStore({
    createStateFn: true
})

export default {
    state: activities.state,
    mutations: activities.mutations,
    actions: activities.actions,
    getters: activities.getters
}
