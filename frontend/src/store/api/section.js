import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const sections = new Vapi({
    baseURL: '/api',
    state: {
        sections: []
    }
}).get({
    action: 'show_section',
    property: 'sections',
    path: ({id}) => `/section/${id}.json`
}).getStore({
    createStateFn: true
})

export default {
    state: sections.state,
    mutations: sections.mutations,
    actions: sections.actions,
    getters: sections.getters
}
