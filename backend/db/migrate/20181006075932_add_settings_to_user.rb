class AddSettingsToUser < ActiveRecord::Migration[5.2]
    def change
        add_column :users, :setting_unit, :integer, default: 0, null: false
        add_column :users, :setting_default_privacy, :integer, default: 0, null: false
        add_column :users, :setting_default_sport, :integer, default: 0, null: false
    end
end
