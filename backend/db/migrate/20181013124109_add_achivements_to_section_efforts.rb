class AddAchivementsToSectionEfforts < ActiveRecord::Migration[5.2]
    def change
        add_column :section_efforts, :achievement, :integer
        add_column :section_efforts, :attempt_number, :integer
    end
end
