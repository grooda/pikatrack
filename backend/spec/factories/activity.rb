FactoryBot.define do
    factory :activity do
        association :user, factory: :user
        activity_type {:cycling}
    end

    factory :activity_fit_file, class: 'Activity' do
        association :user, factory: :user
        activity_type {:cycling}
        original_activity_log_file {
            Rack::Test::UploadedFile
                .new('spec/files/example_fit_file.fit', 'application/vnd.ant.fit')
        }
    end

    factory :activity_gpx_file, class: 'Activity' do
        association :user, factory: :user
        activity_type {:cycling}
        original_activity_log_file {
            Rack::Test::UploadedFile
                .new('spec/files/example_gpx_file.gpx', 'application/gpx+xml')
        }
    end

    # This contains a larger gpx file
    factory :activity_fox_gpx, class: 'Activity' do
        association :user, factory: :user
        activity_type {:cycling}
        original_activity_log_file {
            Rack::Test::UploadedFile
                .new('spec/files/fox_creek.gpx', 'application/gpx+xml')
        }
    end

    factory :activity_split_trkseg, class: 'Activity' do
        association :user, factory: :user
        activity_type {:cycling}
        original_activity_log_file {
            Rack::Test::UploadedFile
                .new('spec/files/fox_creek_split_trksegs.gpx', 'application/gpx+xml')
        }
    end
end
