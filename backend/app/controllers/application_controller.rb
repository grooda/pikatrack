class ApplicationController < ActionController::API
    include DeviseTokenAuth::Concerns::SetUserByToken
    include ApplicationHelper
    before_action :configure_permitted_parameters, if: :devise_controller?


    def current_user
      current_api_user
    end
    protected

    def configure_permitted_parameters
      added_attrs = [:username, :email, :password, :password_confirmation]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end
end
