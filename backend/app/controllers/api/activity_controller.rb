class Api::ActivityController < ApplicationController
    before_action :authenticate_api_user!, only: [:create, :destroy]

    def index
        @activities = Activity.visible(current_api_user).includes(:computed_activity)
            .where(user_id: params[:id]).where.not(computed_activities: {id: nil})
            .order(created_at: :desc)
    end

    def show
        @activity = Activity.find_by(id: params[:id])
        authorize! :read, @activity
        @write_access = can? :write, @activity
        if @activity.nil?
            render :nothing => true, :status => 404
        else
            render json: {error: 'activity still processing'} unless @activity.computed_activity
        end
    end

    def create
        @activity = Activity.new(activity_params.merge({user: current_api_user}))
        if @activity.valid?
            @activity.save
            ActivityWorker.perform_async(@activity.id)
            render 'api/activity/create', formats: [:json]
        else
           render json: {error: @activity.errors}, status: :bad_request
        end
    end

    def destroy
        activity = Activity.find(params[:id])
        authorize! :write, activity
        activity.destroy
    end

    def recompute
        activity = Activity.find_by(id: params[:id])
        authorize! :write, activity
        success = false
        success = activity.recompute
    end

    def activity_params
        params.require(:activity).permit(:title, :description, :privacy, :activity_type, :original_activity_log_file)
    end
end
