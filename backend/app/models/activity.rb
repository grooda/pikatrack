class Activity < ApplicationRecord
    belongs_to :user
    has_many :section_efforts
    has_one :computed_activity
    has_one_attached :original_activity_log_file

    before_save :replace_blank_title
    validate :check_file_presence

    enum privacy: [:public_activity, :friends_activity, :private_activity]
    enum activity_type: ACTIVITY_TYPES

    # TODO: Handle friends when friends feature exists
    scope :visible, -> (user_id) { where("user_id = ? OR privacy = ?", user_id, self.privacies[:public_activity])}

    def replace_blank_title
        self.title = "Untitled activity" if self.title.blank?
    end

    def check_file_presence
        errors.add(:original_activity_log_file, "no file added") unless original_activity_log_file.attached?
    end

    def recompute
        self.computed_activity&.destroy
        ActivityWorker.perform_async(self.id)
        self.computed_activity.destroyed?
    end
end
