require 'open-uri'

class ComputedActivity < ApplicationRecord
    belongs_to :activity
    has_one_attached :activity_log_file

    def save_matching_sections
        SectionEffort.where(activity_id: self.activity.id).delete_all
        matching_sections.each do |matched_section|
            SectionEffort.create(activity_id: self.activity.id,
                                 user_id: self.activity.user.id,
                                 section_id: matched_section[:id],
                                 time: matched_section[:match_end].time - matched_section[:match_start].time)
        end
    end

    def matching_sections
        candidate_section_efforts # TODO: Run through path_compare
    end
    
    def activity_bounds
        bounds = {
            lat_min: gpx.tracks.first.segments.first.points.first.lat,
            lat_max: gpx.tracks.first.segments.first.points.first.lat,
            lon_min: gpx.tracks.first.segments.first.points.first.lon,
            lon_max: gpx.tracks.first.segments.first.points.first.lon
        }

        gpx.tracks.each do |track|
            track.segments.each do |segment|
                segment.points.each do |point|
                    bounds[:lat_min] = point.lat if  point.lat < bounds[:lat_min]
                    bounds[:lat_max] = point.lat if  point.lat > bounds[:lat_max]
                    bounds[:lon_min] = point.lon if  point.lon < bounds[:lon_min]
                    bounds[:lon_min] = point.lon if  point.lon > bounds[:lon_min]
                end
            end
        end
        bounds
    end

    def gpx
        return @gpx if @gpx
        @gpx = GPX::GPXFile.new(gpx_data: open(Rails.application.routes.url_helpers.rails_blob_url(self.activity_log_file)))
    end

    private


        def candidate_sections
            bounds = activity_bounds

            Section.where("ST_Contains(ST_Transform(ST_MakeEnvelope(?,?,?,?,3785),3785), sections.path)",
                                       bounds[:lon_min] - 0.5, bounds[:lat_min] - 0.5, bounds[:lon_min] + 0.5, bounds[:lat_max] + 0.5)
        end

        def candidate_section_efforts
            candidate_states = []
            candidate_sections.each do |candidate|
                candidate_states << {
                    id: candidate.id,
                    path: candidate.path,
                    start: candidate.path.points.first,
                    end: candidate.path.points.last,
                    match_start: nil,
                    match_end: nil
                }
            end

            gpx.tracks.first.segments.first.points.each do |point| # TODO: See if this can be done faster
                candidate_states.each_with_index do |candidate, index|
                    if candidate_states[index][:match_start] == nil
                        distance = Math.sqrt((candidate[:start].x - point.lon) ** 2) + Math.sqrt((candidate[:start].y - point.lat) ** 2)
                        if distance < 0.0002 # Arbitrary value of closeness. Tweak for best results.
                            candidate_states[index][:match_start] = point
                        end
                    elsif candidate_states[index][:match_end] == nil && candidate_states[index][:match_start] != nil
                        distance = Math.sqrt((candidate[:end].x - point.lon) ** 2) + Math.sqrt((candidate[:end].y - point.lat) ** 2)
                        if distance < 0.0002 # Arbitrary value of closeness. Tweak for best results.
                            candidate_states[index][:match_end] = point
                        end
                    end
                end
            end

            candidate_states.reject{|c| c[:match_start].nil? || c[:match_end].nil?}
        end
end
