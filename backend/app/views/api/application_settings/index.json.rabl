collection @application_settings
attributes :code, :text_value, :bool_value, :number_value
node(:setting_type) {|setting| setting.setting_type}
